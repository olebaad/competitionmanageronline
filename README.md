# BoxingTournamentWebApp
An ASP.NET Core Web Application (MVC) that allows a user to manage information about boxers in a tournament.

## Functionalities
- User is able to add and delete a competitor from the database using a form.
- User is able to view a single competitor's information.
- User is able to view all the competitors' information currently in the database.

## Controllers 
- **HomeController**
Based on skeleton build of project, only redirects to BoxersController.

- **BoxersController**
Controles communication with database and routing within application.

## Models
- **Boxers**
Model containing basic information on a competitor.

## Views
### Boxers
- **Create**
Interface allowing user to enter a new competitor to database. 
Input validation is active, so all invormation must be entered in order to allow creation of an entry.
- **Delete**
Interface allowing user to delete a particular entry.
- **Details**
View diaplaying detailed information about a single competitor
- **Edit**
View giving user ability to edit existing entries in table.
- **Index**
Front page displaying all competitors in database.
- **Privacy**
Simple view not displaying any proper information at this point.

### Home
- **Index**
Not in use. Created by framework.
- **Privacy**
Not in use. Created by framework.
