﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BoxingTournamentWebApp.Data;
using BoxingTournamentWebApp.Models;

namespace BoxingTournamentWebApp.Controllers
{
    public class BoxersController : Controller
    {
        private readonly BoxingTournamentWebAppContext _context;

        public BoxersController(BoxingTournamentWebAppContext context)
        {
            _context = context;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        // GET: Boxers
        public async Task<IActionResult> Index()
        {
            return View(await _context.Boxer.ToListAsync());
        }

        // GET: Boxers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boxer = await _context.Boxer
                .FirstOrDefaultAsync(m => m.Id == id);
            if (boxer == null)
            {
                return NotFound();
            }

            return View(boxer);
        }

        // GET: Boxers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Boxers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,WeightInKg")] Boxer boxer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(boxer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(boxer);
        }

        // GET: Boxers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boxer = await _context.Boxer.FindAsync(id);
            if (boxer == null)
            {
                return NotFound();
            }
            return View(boxer);
        }

        // POST: Boxers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,WeightInKg")] Boxer boxer)
        {
            if (id != boxer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(boxer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoxerExists(boxer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(boxer);
        }

        // GET: Boxers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boxer = await _context.Boxer
                .FirstOrDefaultAsync(m => m.Id == id);
            if (boxer == null)
            {
                return NotFound();
            }

            return View(boxer);
        }

        // POST: Boxers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var boxer = await _context.Boxer.FindAsync(id);
            _context.Boxer.Remove(boxer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BoxerExists(int id)
        {
            return _context.Boxer.Any(e => e.Id == id);
        }
    }
}
