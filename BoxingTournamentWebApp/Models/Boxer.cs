﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BoxingTournamentWebApp.Models
{
    public class Boxer
    {
        [Required(ErrorMessage = "Please enter a number")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter firstname")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter lastname")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter weight in kg")]
        public decimal WeightInKg { get; set; }


    }
}