﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BoxingTournamentWebApp.Models;

namespace BoxingTournamentWebApp.Data
{
    public class BoxingTournamentWebAppContext : DbContext
    {
        public BoxingTournamentWebAppContext (DbContextOptions<BoxingTournamentWebAppContext> options)
            : base(options)
        {
        }

        public DbSet<BoxingTournamentWebApp.Models.Boxer> Boxer { get; set; }
    }
}
